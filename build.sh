#!/usr/bin/env bash

echo " --> Running Emerge Web Synchronization"
emerge-webrsync
echo " --> Installing GoLang"
emerge -j4 go
echo " --> Cleaning up"
rm /build.sh
rm -fr /usr/portage/*
rm -fr /tmp/*
rm -fr /var/tmp/*
#rm -fr /usr/share/man/*
#rm -fr /usr/share/sgml/*
#rm -fr /usr/share/doc/*
#rm -fr /usr/share/gtk-doc/*
